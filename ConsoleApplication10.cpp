#include <iostream>
#include <iomanip> 
#include "windows.h"
#include "math.h"

using namespace std;

int main()
 {
	srand(time(NULL));
	const int n = 4;
	const int m = 4;
	int k, max = 0;
	int A[n][m];
	cout << "Enter k:  ";
	cin >> k;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			A[i][j] = rand() % 101 - 50;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cout << setw(5) << A[i][j];
		};
		cout << endl;
	};
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			if (abs(max) < abs(A[i][j]))
			{
				max = abs(A[i][j]);
			}
		}
	}
	cout << "max=" << max << endl;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			swap(max, A[k][k]);
			cout << setw(5) << A[i][j];
		}
		cout << endl;
	}
} 
